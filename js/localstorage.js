// Variables Locales
const content = document.querySelector('#form'); // Guardamos un elemento en una variable
const list = document.getElementById('list');  // Otra forma de hacer lo de arriba
let arrayList = []; // Creamos un Array vacio


// Funciones
const crearItem = (tweet) => { // Funcion para crear el contenido de la lista
    let item = {tweet: tweet} // variable que crea el contenido
    arrayList.push(item); // insertamos dicha variable en el array ya creado
    return item; // Nos devuelve el resultado de dicha creacion
}

const saveContent = () => {
    localStorage.setItem('tweet', JSON.stringify(arrayList)); // Guardamos los datos en localstorage, el JSON es para traducir la array en un string
    loadContent(); // cada vez que guarde un elemento que se actualice la lista
    
}

const loadContent = () => {
    list.innerHTML = ''; // vaciamos la lista
    arrayList = JSON.parse(localStorage.getItem('tweet')); // Llamamos a los elementos strings y los pasamos a array
    if(arrayList === null){ // si no llegan datos...
        arrayList = []; // declarar el array vacio
    } else { // sino...
        arrayList.forEach(element => { // Recorremos el array
            list.innerHTML += `<li class="d-flex justify-content-between">${element.tweet}<i class="fas fa-times"></i></li>`; // Imprimimos el array en html
        });
    }
}

const deleteContent = (tweet) => {
    let indexArray;
    arrayList.forEach((element, index) => { // Extraemos el index del array
        if (element.tweet === tweet) { // Comparamos el elemento almacenado con el que hicimos click
            indexArray=index; // Guardamos el resultado en una variable
        }
    });
   arrayList.splice(indexArray,1) // Ejecutamos una eliminacion
   saveContent(); // Ejecutamos la funcion "Guardar y actualizar"
}

// EventListener
content.addEventListener('submit', (e) => { // Al hacer click en submit ejecuta la funcion
    e.preventDefault(); // Previene que se ejecute otros eventos
    let saveText = document.querySelector('#textarea').value; // Guarda en una variable el valor de textarea
    
    crearItem(saveText); // Ingresamos dicho valor a la funcion del array
    saveContent(); // Ejecutamos la funcion "Guardar y actualizar"

    content.reset(); // Vaciamos el campo
});

document.addEventListener('DOMContentLoaded', loadContent); // Este evento se ejecuta cuando el DOM termian de cargar

list.addEventListener('click', (e) => { // Con console.log(e); vemos que ejecuta el click
    e.preventDefault();
    if(e.path[0].nodeName === 'path'){ // Identificamos el elemento clickeado
    let element = e.path[2].childNodes[0].nodeValue; // Identificamos el elemento clickeado y extraemos su valor
    deleteContent(element); // Insertamos el valor en la funcion eliminar
    }
});